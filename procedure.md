Procedure
=========

1. Put olive oil and garlic in a **frying pan**.
2. Dice onion and add fry until translucent.
3. Chop bell peppers and tomatoes and add to pan.
4. Add salt, curry powder, (optional) turmeric, chilli powder/cayenne pepper.
5. Let cook until tomatoes are done and crack eggs on top.
