Ingredients
===========

* Olive oil
* Garlic
* Onions

* Bell peppers
* Tomatoes (3 units)

* Salt
* Curry powder
* (optional) Turmeric
* Red chilli powder/cayenne pepper

* Eggs (4 units)
